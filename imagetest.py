import platform
import matplotlib.pyplot as plt
from pandas.core.common import flatten
from facenet_pytorch import MTCNN, InceptionResnetV1
from PIL import Image, ImageDraw

import array
import copy
import numpy as np
import random
import os.path

import torch
from torch import nn
from torch import optim
import torch.nn.functional as F
from torchvision import datasets, transforms, models, utils
from torch.utils.data import Dataset, DataLoader
from torchvision.utils import make_grid
from torchvision.transforms import ToPILImage

import albumentations as A
from albumentations.pytorch import ToTensorV2
import cv2

import glob
import timeit

model_path = './family_img_net.pth'
train_data_path = 'images/train' 
test_data_path = 'images/test'

initial_test_image_path = 'images/train/gene_denman/Screenshot 2023-02-25 at 2.06.48 PM.png'

performTraining = True
doInitialClassificationTest = False
letUserInputFileNames = True
useVideoSourceForClassification = False

print(platform.platform())

mps_device = torch.device("cpu")
mtcnn_device = torch.device("cpu")
force_cpu_device = False


from torchvision.transforms import ToTensor

class ToDeviceTransform:
    def __init__(self, device):
        self.device = device
        
    def __call__(self, image):
        return image.to(device=self.device)

#######################################################
#               Define Transforms
#######################################################

#To define an augmentation pipeline, you need to create an instance of the Compose class.
#As an argument to the Compose class, you need to pass a list of augmentations you want to apply. 
#A call to Compose will return a transform function that will perform image augmentation.
#(https://albumentations.ai/docs/getting_started/image_augmentation/)

train_transforms = A.Compose(
    [
        A.SmallestMaxSize(max_size=200),
        A.CenterCrop(height=160, width=160),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2()
    ]
)
train_transforms2 = A.Compose(
    [
        A.SmallestMaxSize(max_size=200),
        A.ShiftScaleRotate(shift_limit=0.05, scale_limit=0.05, rotate_limit=360, p=0.5),
        A.RandomCrop(height=160, width=160),
        A.RGBShift(r_shift_limit=15, g_shift_limit=15, b_shift_limit=15, p=0.5),
        A.RandomBrightnessContrast(p=0.5),
        A.MultiplicativeNoise(multiplier=[0.5,2], per_channel=True, p=0.2),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        A.HueSaturationValue(hue_shift_limit=0.2, sat_shift_limit=0.2, val_shift_limit=0.2, p=0.5),
        A.RandomBrightnessContrast(brightness_limit=(-0.1,0.1), contrast_limit=(-0.1, 0.1), p=0.5),
        ToTensorV2()
    ]
)

test_transforms = A.Compose(
    [
        A.SmallestMaxSize(max_size=200),
        A.CenterCrop(height=160, width=160),
        A.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)),
        ToTensorV2()
    ]
)


####################################################
#       Create Train, Valid and Test sets
####################################################

train_image_paths = [] #to store image paths in list
classes = [] #to store class values
test_image_paths = []
valid_image_paths = []

def setup_training_data():
    global train_image_paths, classes, valid_image_paths
    global test_image_paths

    test_image_paths = []
    valid_image_paths = []
    train_image_paths = []
    classes = []
    #1.
    # get all the paths from train_data_path and append image paths and class to to respective lists
    # eg. train path-> 'images/train/26.Pont_du_Gard/4321ee6695c23c7b.jpg'
    # eg. class -> 26.Pont_du_Gard
    for data_path in glob.glob(train_data_path + '/*'):
        classes.append(data_path.split('/')[-1]) 
        train_image_paths.append(glob.glob(data_path + '/*'))

    for data_path in glob.glob(test_data_path + '/*'):
        train_image_paths.append(glob.glob(data_path + '/*'))
    
    classes.append("unknown")
    train_image_paths = list(flatten(train_image_paths))
    random.shuffle(train_image_paths)
    setup_classes(classes)

    print('train_image_path example: ', train_image_paths[0])
    print('class example: ', classes[0])

    #2.
    # split train valid from train paths (80,20)
    train_image_paths, valid_image_paths = train_image_paths[:int(0.8*len(train_image_paths))], train_image_paths[int(0.8*len(train_image_paths)):] 

def setup_testing_data():
    global test_image_paths, classes
    for data_path in glob.glob(test_data_path + '/*'):
        classes.append(data_path.split('/')[-1]) 
        test_image_paths.append(glob.glob(data_path + '/*'))

    test_image_paths = list(flatten(test_image_paths))
    setup_classes(classes)
    print("Train size: {}\nValid size: {}\nTest size: {}".format(len(train_image_paths), len(valid_image_paths), len(test_image_paths)))


#######################################################
#      Create dictionary for class indexes
#######################################################
idx_to_class = 0
class_to_idx = 0
def setup_classes(cls):
    global idx_to_class, class_to_idx
    if(idx_to_class == 0):
        idx_to_class = {i:j for i, j in enumerate(cls)}
        class_to_idx = {value:key for key,value in idx_to_class.items()}
        print(idx_to_class)



#######################################################
#               Define Dataset Class
#######################################################

class ImagingDataset(Dataset):
    def __init__(self, image_paths, transform=False):
        self.image_paths = image_paths
        self.transform = transform
        self.images = [0] * len(image_paths)
        self.disp_images = [0] * len(image_paths)
        
    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        global train_transforms2
        image_filepath = self.image_paths[idx]
        label = image_filepath.split('/')[-2]
        label = class_to_idx[label]

        if idx >= len(self.images) or type(self.images[idx]) != torch.Tensor:
            image = cv2.imread(image_filepath)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            #if self.transform is not None:
            #    image = self.transform(image=np.array(image))["image"]
            faces = setupfaces(image)
            for face in faces:
                if self.transform is not None:
                    face = self.transform(image=np.array(face[0]))["image"]
                if idx >= len(self.images):
                    self.images.resize(idx+1)
                self.images[idx] = face
                break

        img = self.images[idx] #train_transforms2(image=self.images[idx].numpy())["image"]
        if isinstance(img, torch.Tensor):
            img = img.to(mps_device)
        return img, label
    

class ClassifyDataset(Dataset):
    def __init__(self, imagePath, transform=False):
        self.imagePath = imagePath
        self.transform = transform
        self.images = [0]
        self.disp_images = [0]
        self.use_disp_images = False
        self.lenth = 0
        self.setupFaces()
        
    def __len__(self):
        return self.lenth or 1

    def __getitem__(self, idx):
        print(f"get item request index {idx} but have length of {self.lenth} and array length of {len(self.images)}")
        image = self.images[idx]
        if self.use_disp_images:
            image = self.disp_images[idx]

        #image = Image.fromarray(image)
        #image = cv2.imread(self.imagePath)
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if isinstance(image, torch.Tensor):
            image = image.to(mps_device)
        return image, 0
    
    def setupFaces(self):
        image_filepath = self.imagePath

        image = cv2.imread(image_filepath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        faces = setupfaces(image, True)
        idx = 0
        self.images = [0] * len(faces)
        for face in faces:
            #image = Image.fromarray(face[0])
            #face = Image.fromarray(face)
            displen = len(self.disp_images)
            if idx >= displen:
                self.disp_images = self.disp_images + [0] * (idx +1 - displen)
            self.disp_images[idx] = face[0].copy()
            if self.transform is not None:
                face = self.transform(image=face[0])["image"]
            if idx >= len(self.images):
                self.images.resize(idx+1)
            self.images[idx] = face
            idx += 1

        self.lenth = idx
        print(f"after faces length is {self.lenth}")

class VideoFeedDataset(Dataset):
    def __init__(self, videoSourceIndex, personIndex, transform=None):
        self.videoSourceIndex = videoSourceIndex
        self.transform = transform
        self.vid = None
        self.personIndex = personIndex
        
    def __len__(self):
        return 1000000

    def __getitem__(self, idx):
        if self.vid is None:
            self.vid = cv2.VideoCapture(self.videoSourceIndex)

        ret, image = self.vid.read()

        #image = Image.fromarray(image)
        #image = cv2.imread(self.imagePath)
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        faces = setupfaces(image)
        for face in faces:
            if self.transform is not None:
                image = np.array(face[0])
            break

        if isinstance(image, torch.Tensor):
            image = image.to(mps_device)
        image = self.transform(image=image)["image"]
        return image, self.personIndex
    
    def setupFaces(self):
        image_filepath = self.imagePath

        image = cv2.imread(image_filepath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        faces = setupfaces(image, True)
        idx = 0
        self.images = [0] * len(faces)
        for face in faces:
            #image = Image.fromarray(face[0])
            #face = Image.fromarray(face)
            displen = len(self.disp_images)
            if idx >= displen:
                self.disp_images = self.disp_images + [0] * (idx +1 - displen)
            self.disp_images[idx] = face[0].copy()
            if self.transform is not None:
                face = self.transform(image=face[0])["image"]

            idx += 1

        self.lenth = idx
        print(f"after faces length is {self.lenth}")


#######################################################
#                  Visualize Dataset
#         Images are plotted after augmentation
#######################################################

def visualize_augmentations(dataset, idx=0, samples=30, cols=10, random_img = False, labels = False): 
    global idx_to_class
    #dataset.use_disp_images = True
    dataset = copy.deepcopy(dataset)
    #dataset.use_disp_images = False
    if hasattr(dataset, "transform"):
        #we remove the normalize and tensor conversion from our augmentation pipeline
        dataset.transform = A.Compose([t for t in dataset.transform if not isinstance(t, (A.Normalize, ToTensorV2, ToDeviceTransform))])

    if cols > samples:
        cols = samples
    if cols == 1:
        cols = 2
        
    rows = samples // cols
    if rows == 0:
        rows = 1
    
    figure, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(12, 8))
    for i in range(samples):
        idx = i
        if random_img:
            idx = np.random.randint(1,len(dataset))
        image, lab = dataset[idx]
        if isinstance(labels, torch.Tensor):
            lab = labels[i].item()

        if type(image) == torch.Tensor:
            image = ToPILImage()(image)
        ax.ravel()[i].imshow(image)
        ax.ravel()[i].set_axis_off()
        ax.ravel()[i].set_title(idx_to_class[lab])

    plt.tight_layout(pad=1)
    plt.show()    


def imshow(images):
    if True:
        return
    
    figure, ax = plt.subplots(nrows=5, ncols=2, figsize=(12, 8))
    for i in range(images):
        image, lab = dataset[idx]
        
        ax.ravel()[i].imshow(image)
        ax.ravel()[i].set_axis_off()
        ax.ravel()[i].set_title(idx_to_class[lab])
    plt.tight_layout(pad=1)
    plt.show()    

    #img = img / 2 + 0.5     # unnormalize
    #npimg = img.numpy()
    #plt.imshow(np.transpose(npimg, (1, 2, 0)))
    #plt.show()


def imshow2(img):
    if True:
        return
    
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


#######################################################
#               Define Neural Network
#######################################################
class FaceIdentification(InceptionResnetV1): #(nn.Module):
    
    def training_step(self, batch):
        images, labels = batch 
        labels = labels.to(mps_device)
        out = self(images)    
                                            # Generate predictions
        loss = F.cross_entropy(out, labels) # Calculate loss
        return loss
    
    def validation_step(self, batch):
        images, labels = batch 
        labels = labels.to(mps_device)
        out = self(images)                    # Generate predictions
        loss = F.cross_entropy(out, labels)   # Calculate loss
        acc = accuracy(out, labels)           # Calculate accuracy
        return {'val_loss': loss.detach(), 'val_acc': acc}
        
    def validation_epoch_end(self, outputs):
        batch_losses = [x['val_loss'] for x in outputs]
        epoch_loss = torch.stack(batch_losses).mean()   # Combine losses
        batch_accs = [x['val_acc'] for x in outputs]
        epoch_acc = torch.stack(batch_accs).mean()      # Combine accuracies
        return {'val_loss': epoch_loss.item(), 'val_acc': epoch_acc.item()}
    
    def epoch_end(self, epoch, result):
        print("Epoch [{}], train_loss: {:.4f}, val_loss: {:.4f}, val_acc: {:.4f}".format(
            epoch, result['train_loss'], result['val_loss'], result['val_acc']))

def accuracy(outputs, labels):
    _, preds = torch.max(outputs, dim=1)
    return torch.tensor(torch.sum(preds == labels).item() / len(preds))

  
@torch.no_grad()
def evaluate(model, val_loader):
    model.eval()
    outputs = [model.validation_step(batch) for batch in val_loader]
    return model.validation_epoch_end(outputs)

  
def fit(epochs, lr, model, train_loader, val_loader, opt_func = torch.optim.SGD):
    
    history = []
    optimizer = opt_func(model.parameters(),lr)
    for epoch in range(epochs):
        
        model.train()
        train_losses = []
        for batch in train_loader:
            loss = model.training_step(batch)
            train_losses.append(loss)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
            
        result = evaluate(model, val_loader)
        result['train_loss'] = torch.stack(train_losses).mean().item()
        model.epoch_end(epoch, result)
        history.append(result)
    
    return history


def plot_accuracies(history):
    """ Plot the history of accuracies"""
    accuracies = [x['val_acc'] for x in history]
    plt.plot(accuracies, '-x')
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.title('Accuracy vs. No. of epochs');
    

def plot_losses(history):
    """ Plot the losses in each epoch"""
    train_losses = [x.get('train_loss') for x in history]
    val_losses = [x['val_loss'] for x in history]
    plt.plot(train_losses, '-bx')
    plt.plot(val_losses, '-rx')
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])
    plt.title('Loss vs. No. of epochs');

def setupfaces(image, showFaces=False):
    global train_transforms
    #image = cv2.resize(image, (0,0), fx=0.5, fy=0.5)
    image_draw = Image.fromarray(image)
    boxes, _ = mtcnn.detect(image_draw)
    images = []
    dspImages = []

    if boxes is not None:
        images = [0] * len(boxes)
        dspImages = [0] * len(boxes)
        draw = ImageDraw.Draw(image_draw)
    
        pos = 0
        padding = 10
        if boxes is not None:
            for box in boxes:
                x1, y1, x2, y2 = box.astype(int)
                width = x2 - x1
                height = y2 - y1
                size = width if width > height else height
                wdiff = size - width
                wextra = wdiff/2 + size/12
                hdiff = size - height
                hextra = hdiff/2 + size/12
                        
                ax1 = max(0, x1 - wextra)
                ay1 = max(0, y1 - hextra)
                ax2 = min(image_draw.size[0], x2 + wextra)
                ay2 = min(image_draw.size[1], y2 + hextra)
                """             
                if ax1 > ax2:
                    t = ax2
                    ax2 = ax1
                    ax1 = t
                if ay1 > ay2:
                    t = ay2
                    ay2 = ay1
                    ay1 = t
                """
                try:
                    image = image_draw.crop([ax1,ay1,ax2,ay2])
                    #image  = image_draw #np.asarray(image_draw)
                    npimg = np.array(image)
                    dspImage = Image.fromarray(npimg)
                    dspImages[pos] = [dspImage,0]
                    images[pos] = [npimg,0]
                    pos += 1
                except:
                    print("error")
                #image = dataset.transform(image=tens)["image"]
                #draw.rectangle(box.tolist(), outline=(255, 0, 0), width=6)
                if showFaces == False:
                    break
    
    #if showFaces == True:
    #    visualize_augmentations(dspImages, 0, pos,10,False)

    return images



def do_training(net, num_epochs, training_transform, visualize = False):
    global model_path
    global train_image_paths,train_transforms,train_transforms2
    global valid_image_paths,test_transforms,train_transforms2
    train_dataset = ImagingDataset(train_image_paths,training_transform)
    valid_dataset = ImagingDataset(valid_image_paths,test_transforms) #test transforms are applied

    #print('The shape of tensor for 50th image in train dataset: ',train_dataset[2][0].shape)
    #print('The label for 50th image in train dataset: ',train_dataset[2][1])

    if visualize:
        visualize_augmentations(train_dataset,np.random.randint(1,len(train_image_paths)), random_img = True)
    #visualize_augmentations(valid_dataset,10, random_img = True)
    net.train()

    train_loader = DataLoader(
        train_dataset, batch_size=32, shuffle=True
    )

    valid_loader = DataLoader(
        valid_dataset, batch_size=32, shuffle=True
    )

    opt_func = torch.optim.Adam
    lr = 0.001
    #fitting the model on training data and record the result after each epoch
    history = fit(num_epochs, lr, net, train_loader, valid_loader, opt_func)
    #plot_accuracies(history)
    #plot_losses(history)
    #plt.show()
    print('Finished Training')
    torch.save(net.state_dict(), model_path)


@torch.no_grad()
def classify_image(net, image_filepath):
    global classes
    net.eval()
    dataset = ClassifyDataset(image_filepath,test_transforms)
    loader = DataLoader(
        dataset, batch_size=32, shuffle=False
        )

    for batch in loader:
        images, labels = batch 
        images = images.to(mps_device)
        results = net(images)  
        pos = 0
        if len(images) > 1:
            pos = 1      
        #imshow2(images[pos])
        _, preds = torch.max(results, 1)

    del dataset.transform
    dataset.use_disp_images = True
    visualize_augmentations(dataset, 0, len(images),10,False, preds)
    dataset.use_disp_images = False
    #accr = torch.tensor(torch.sum(preds == labels).item() / len(preds))
    print('Predicted: ', ' '.join(f'{j} {classes[preds[j]]:5s} ({_[j]})' for j in range(preds.size()[0])))
    return classes[preds[0]]

def test_model():
    net.eval()
    test_dataset = ImagingDataset(test_image_paths,test_transforms)
    test_loader = DataLoader(
        test_dataset, batch_size=32, shuffle=False
    )
    dataiter = iter(test_loader)
    images, labels = next(dataiter)
    labels = labels.to(mps_device)
    images = images.to(mps_device)

    #print('GroundTruth: ', ' '.join(f'{j} {classes[labels[j]]:5s}' for j in range(labels.len())))
    #print('GroundTruth: ', ' '.join(f'{j} {classes[labels[j]]:5s}' for j in range(8,16)))
    #print('GroundTruth: ', ' '.join(f'{j} {classes[labels[j]]:5s}' for j in range(16,24)))
    #imshow2(make_grid(images)) 

    outputs = net(images)
    _, predicted = torch.max(outputs, 1)

    F.cross_entropy(outputs, labels)
    print('Predicted: ', ' '.join(f'{j} {classes[predicted[j]]:5s} : {classes[labels[j]]:5s}\n' for j in range(labels.size()[0])))
    #print('Predicted: ', ' '.join(f'{j} {classes[predicted[j]]:5s}' for j in range(8,16)))
    #print('Predicted: ', ' '.join(f'{j} {classes[predicted[j]]:5s}' for j in range(16,24)))


# Use the video source to grab frames, do facial recognition and render frames
# as fast as possible.
def classify_from_video(net):
    videoSource = 0
    videoDataset = VideoFeedDataset(videoSource, 14, test_transforms)
    videoTrainloader = DataLoader(
            videoDataset, batch_size=12, shuffle=False
            )

    opt_func = torch.optim.Adam
    lr = 0.001
    #fitting the model on training data and record the result after each epoch
    history = fit(10, lr, net, videoTrainloader, videoTrainloader, opt_func)


    vid = cv2.VideoCapture(1)  
    detectRate = 4
    currentFrame = 0
    boxes = None

    while(True):
        
        # Capture the video frame
        # by frame
        ret, frame = vid.read()

        if currentFrame == 0 or currentFrame % detectRate == 0:
            newboxes, _ = mtcnn.detect(frame)
            if newboxes is not None:
                boxes = newboxes
            
            # Draw faces
        if boxes is not None:
            frame =  Image.fromarray(frame)
            draw = ImageDraw.Draw(frame)
            for box in boxes:
                draw.rectangle(box.tolist(), outline=(255, 0, 0), width=6)
            frame =np.asarray(frame)

        currentFrame += 1
        # Display the resulting frame
        cv2.imshow('frame', frame)
        
        # the 'q' button is set as the
        # quitting button you may use any
        # desired button of your choice
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    # After the loop release the cap object
    vid.release()
    # Destroy all the windows
    cv2.destroyAllWindows()


# Allow user to do ad-hock testing against arbitrary images as long as the file name pass is accessable
# the images used here can have multiple people in it.  If you trained against 100 people and 10 of them
# are standing in a group photo, this will recognize the 10 faces and do facial recognition against them.
def classify_from_cli_prompt(net):
    while True:
        user_input = input("Enter accessable file system file name to classify (q to quit): ")
        if user_input == "q":
            break
        else:
            result = classify_image(net, user_input)
            print(result)


# Training against the images folder.  Using 20 with resnet seems to be good
# but experiment with more epoches or doing it again as shown in commented out section
# bellow
def do_fixed_training_from_images_folder(net):
    do_training(net, 20, train_transforms)
    do_training(net, 20, train_transforms2, False)
    """
    do_training(net, 50, train_transforms)
    do_training(net, 50, train_transforms2, False)

    do_training(net, 30, train_transforms)
    do_training(net, 50, train_transforms2, False)


    do_training(net, 30, train_transforms)
    do_training(net, 50, train_transforms2, False)
    

    setup_testing_data()
    test_model()
    """


#######################################################
#  Beginning of direct executable code
#######################################################

# Check for M1 or CUDA device and use that for GPU acceleration otherwise use CPU
if force_cpu_device is False:
    if torch.cuda.is_available():
        mps_device = torch.device("cuda")

    if torch.backends.mps.is_available():
        mps_device = torch.device("mps")

    x = torch.ones(1, device=mps_device)
    print (x)


# create the neural network and MTCNN instance
mtcnn = MTCNN(device=mtcnn_device, margin=0, selection_method="probability",thresholds=[0.7, 0.8, 0.8])
net = FaceIdentification(num_classes=16, classify=True, pretrained='vggface2')
net.to(mps_device)

# load the prior trained model if the model file exists
if os.path.isfile(model_path):
    print ("loading model data")
    net.load_state_dict(torch.load(model_path))

# get basic file system images folder training loaders setup
setup_training_data()


#######################################################
# Depending on values of global variables defined at top of page
# execute the various testing or classification abilities. 
#######################################################

if performTraining:   
    do_fixed_training_from_images_folder(net)


if doInitialClassificationTest:
    start = timeit.default_timer()
    result = classify_image(net, initial_test_image_path)
    end = timeit.default_timer()
    elapsed_time_ms = (end - start) * 1000
    print(f"classified in {elapsed_time_ms:.2f}ms - results: {result}")


if letUserInputFileNames:
    classify_from_cli_prompt(net)


if useVideoSourceForClassification:
    classify_from_video(net)

