# pytorch-image
Facial Recognition from images or video camera

## Description
This project uses python pytorch and torchvision project as the foundation for the Convoluted Neural Net (CNN).
Uses pytorch, torchvision, facenet-pytorch and other python libraries to do facial recognition.

- pytorch - https://pytorch.org/
- torchvision - https://pytorch.org/vision/stable/index.html
- facenet-pytorch - https://github.com/timesler/facenet-pytorch

## Installation
You need a running python 3.7 or greater.  I used 3.8 on macos m1 for this work and have done the work to enable the pytorch gpu acceleration on the m1.  I also run this on an ubuntu server with nvidia cuda gpu acceleration.  The code is written to detect either and use it otherwise use CPU only, which is much slower.  

I've run the testing with a trained model on raspberry pi with 8g ram with attached webcam, though framerate is not as high as with more powerful hardware but still good.

Start with the pytorch getting started - installation guide at https://pytorch.org/get-started/locally/

After getting python 3 and pytorch installed you need to install some other python libs using pip.  I think
I covered them all here, but I may have missed a few.  This is from me not using Anaconda like pytorch guide
recommends.

pip install facenet-pytorch
pip install albumentations
pip install pandas
pip install matplotlib


## Usage
This first version requires you to comment out and in sections to control what running the base python app will do.
You can train again a local directory of images which are organized in a specific folder structure as follows:

images -> [test or train] -> [person-name]

for example:
- /pytorch-image/images/test/matt.denman/image1.jpg
- /pytorch-image/images/train/matt.denman/image2.jpg
- /pytorch-image/images/test/ann.denman/test/image3.jpg

There should be a fixed number of person-name folders within the images folder.  In the above example there are only 2 names, but a better size would be dozens to thousands.  The more unique images per test/train folder the better.  At least 50 would be a good start.  You can get by with less but accuracy for video recognition will go down.

- **make sure you have the images/test and images/train folders setup before you run imagetest.py as it will not run properly without those**
- **images in the person-name images/test and images/train folders only have that person in the photo.  You don't need to crop to headshots as it will do face recognition and crop the image it uses for facial recognition to the face crop only.  However, if multiple faces are in the photo or if the face detection thinks there are mulitple faces in the  photo it will cause problems.  I browsed google photo and took screen shots of somewhat around the face and saved directly into the person-name folder for test or train.**

Code at the bottom of the imagetest.py has sections for training, testing against a prompt which you can type a file location of an image to test again, or testing against a live stream video source.

Towards the top of the page are some variables you can set to True or False to control what execution of imagetest.py will do.

- performTraining = True
- doInitialClassificationTest = False
- letUserInputFileNames = True
- useVideoSourceForClassification = False

If both letUserInputFileNames and useVideoSourceForClassification are True the video stream for classification will never run because of the endless loop of gathering input from the user.
